# Custom List Theme

## How To Use

In order to use this theme in your Zero-based project, you need to follow the steps below in order.

 1. Download the theme files
 2. Create a folder under {project root}**/themes/pz-theme-custom-list**
 3. Copy the theme files you downloaded earlier to themes/pz-theme-custom-list folder
 4. Add the following line to the .env file:

****
    THEME='pz-theme-custom-list'

****

 5. Restart the app server and rerun "yarn dev"

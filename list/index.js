import { ApiClient } from 'shop-packages';

import * as actionTypes from 'shop-packages/analytics/action-types';
import { pushEvent } from 'shop-packages/analytics';
import MobileFiltering from '@Theme/list/filters/';
import ProductItem from '@Theme/components/product-item';
import Overlay from '@Theme/components/overlay';

export default class Listing {
  constructor() {
    this.container = $('.list').first();
    this.selectedFilters = [];
    this.selectedSorter = '';
    this.searchText = location.search?.includes('search') ? location.search.split('&')[0].split('=')[1] : '';
    this.editMode = false;
    this.page = 0;
    this.init();

    window.onpopstate = history.onpushstate = () => {
      this.fetch(location.pathname + location.search);
    };
  }

  getSelectedFilters(container) {
    const urlSearchParams = new URLSearchParams();
    let selectedFilters = [];

    Array.from(document.querySelector(container).querySelectorAll('.js-sidebar-filter'))
      .filter((filter) => filter.value.length > 0)
      .forEach((filter) => {
        urlSearchParams.set(filter.name, filter.value);
      });

    for (const key of urlSearchParams.keys()) {
      selectedFilters.push({
        name: key,
        value: urlSearchParams.get(key)
      });
    }

    return selectedFilters;
  }

  getSelectedFiltersMobile(container) {
    const selectedFilters = [];

    Array.from(document.querySelector(container).querySelectorAll('.js-filter-choice:checked'))
      .forEach((filter) => {
        selectedFilters.push({
          name: filter.name,
          value: filter.value
        });
      });

    return selectedFilters;
  }

  init() {
    this.selectedFilters = this.getSelectedFilters('.js-list-sidebar');
    this.selectedSorter = '';
    this.mobileFilter = new MobileFiltering(this.selectedFilters);

    new Overlay(this.mobileFilter.close.bind(this.mobileFilter));
    new ProductItem('pz-icon-heart-stroke', 'pz-icon-heart-full');

    const sorterValue = this.container.find('.js-sorter').val();

    if (sorterValue !== 'default') {
      this.selectedSorter = sorterValue;
    }

    $(document).on('click', '.js-product-wrapper', (e) => {
      const productPk = $(e.currentTarget).data('pk');
      const productUrl = $(e.currentTarget).data('url');

      this.pushAnalyticsData(productPk, actionTypes.productClicked);
      e.preventDefault();
      window.location.href = productUrl;
    });

    this.container.find('.js-pagination-item').on('click', (e) => {
      e.preventDefault();

      const $el = $(e.currentTarget);

      if ($el.hasClass('passive') || $el.hasClass('active')) {
        return;
      }

      const page = parseInt($el.text(), 10);

      if (this.page !== page) {
        const href = $el.attr('href');

        this.page = page;

        history.pushState(null, document.title, href);
        this.fetch(href);
      }
    });

    this.container.find('.js-filter-choice').on('change', (e) => {
      const $el = $(e.currentTarget);
      const filterItem = $el.closest('.js-filter-item');

      this.isSingle = filterItem.hasClass('js-filter-exclusive');

      if (this.isSingle) {
        $el.closest('label').siblings().removeClass('-filter-selected');

        $el.closest('label').siblings().each((i, el) => {
          const $elCheckbox = $(el).find('input');

          $elCheckbox.prop('checked', false);
          this.removeFilter($elCheckbox.attr('name'), $elCheckbox.val());
        });
      }
    });

    document.querySelector('.list__sidebar-filter-wrapper [name="attributes_integration_color"]').value=['SİYAH', 'BEYAZ'];

    this.container.find('.js-sidebar-filter').on('change', (e) => {
      const $el = $(e.currentTarget).find('.pz-form-input :selected');
      const name = $el.attr('name');
      const value = $el.attr('value');

      this.addFilter(name, value);
      this.fetchProducts();
    });

    this.container.find('.js-sorter').on('change', (e) => {
      const value = $(e.currentTarget).val();

      if (value === 'default') {
        this.selectedSorter = '';
      } else {
        this.selectedSorter = value;
      }

      this.page = 0;
      this.fetchProducts();
    });

    this.container.find('.js-remove-choice').on('click', (e) => {
      e.preventDefault();

      const btn = $(e.currentTarget);
      const name = btn.data('name').toString();
      const value = btn.data('value').toString();
      const parent = btn.data('parent');

      if (parent) {
        document.location.href = parent;
      } else {
        this.removeFilter(name, value);
      }

      this.fetchProducts();
    });

    this.container.find('.js-clear-all-filters').on('click', (e) => {
      e.preventDefault();
      this.clearFilters();
    });

    this.container.find('.js-open-mobile-filters').on('click', (e) => {
      e.preventDefault();
      this.mobileFilter.open();
    });

    this.container.find('.js-list-apply-filters').on('click', async (e) => {
      e.preventDefault();
      this.selectedFilters = this.getSelectedFiltersMobile('.js-list-sidebar');
      this.page = 0;

      if (this.selectedFilters.length) {
        await this.fetchProducts();
        return;
      }

      this.mobileFilter.close();
    });
  }

  clearFilters() {
    this.selectedFilters = [];
    this.page = 0;
    this.fetchProducts();
  }

  addFilter() {
    const urlSearchParams = new URLSearchParams();
    let selectedFilters = [];

    Array.from(document.querySelector('.js-list-sidebar').querySelectorAll('.js-sidebar-filter'))
      .filter((filter) => filter.value.length > 0)
      .forEach((filter) => {
        if (typeof filter.value === 'object') {
          filter.value.forEach((v) => {
            urlSearchParams.append(filter.name, v);
          });
        } else {
          urlSearchParams.append(filter.name, filter.value);
        }
      });

    for (const key of urlSearchParams.keys()) {
      selectedFilters.push({
        name: key,
        value: urlSearchParams.get(key)
      });
    }

    this.page = 0;
    this.selectedFilters = selectedFilters;
  }

  removeFilter(name, value) {
    this.selectedFilters = this.selectedFilters.filter((selectedFilter) =>
      !(selectedFilter.name === name && selectedFilter.value === value));

    this.page = 0;
  }

  pushAnalyticsData(productPk, actionType) {
    document.querySelectorAll('.analytics-data').forEach((data) => {
      const analyticsData = JSON.parse(data.textContent);

      if (!analyticsData.type) {
        return;
      }

      if (analyticsData.type === actionTypes.productListViewed) {
        const products = analyticsData.payload;

        $.each(products, (index, value) => {
          for (const key in value) {
            if (productPk == value[key]) {
              const analyticsData = {
                type: actionType,
                payload: {
                  products: [value]
                }
              };

              pushEvent(analyticsData);
              return;
            }
          }
        });
      }
    });
  }

  async fetchProducts() {
    let params = [];
    const layout = this.container.data('layout');
    const defaultLayout = this.container.data('defaultLayout');

    if (this.searchText) {
      params.push(
        $.param({
          search_text: decodeURIComponent(this.searchText)
        })
      );
    }

    if (layout && layout != defaultLayout) {
      params.push(
        $.param({
          layout
        })
      );
    }

    for (let filter of this.selectedFilters) {
      params.push(
        $.param({
          [filter.name]: filter.value
        })
      );
    }

    if (this.selectedSorter) {
      params.push(
        $.param({
          sorter: this.selectedSorter
        })
      );
    }

    if (this.page && this.page !== 1) {
      params.push(
        $.param({
          page: this.page
        })
      );
    }

    const query = `${location.pathname}?${params.join('&')}`;

    history.pushState(null, document.title, query);
    await this.fetch(query);
  }

  async fetch(query) {
    const response = await ApiClient.misc.fetchPageHtml(query);
    const isOpenedMobileFiltering = $('.js-list-sidebar').hasClass('-active');
    const result = $(response).find('.list').html();

    this.container.html(result);

    if (isOpenedMobileFiltering) {
      this.container.find('.js-list-sidebar').addClass('-active');
    }

    this.init();

    $('html, body').stop().animate({
      scrollTop: this.container.offset().top
    });
  }
}

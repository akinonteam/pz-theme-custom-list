import { disabled, vRequired } from './../';

export default (BaseClass) =>
  class extends vRequired(disabled(BaseClass)) {
    formInputClassName = 'pz-form-input';

    formInput;

    multipleValues = [];

    static get observedAttributes() {
      const attributes = ['value'];

      return (super.observedAttributes || []).concat(attributes);
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (super.attributeChangedCallback) {
        super.attributeChangedCallback(name, oldValue, newValue);
      }
    }

    get value() {
      if (this.multiple) {
        return this.multipleValues;
      }

      const formInput = this.querySelector(`.${this.formInputClassName}`);

      if (!formInput) {
        return '';
      }

      return this.unmaskedValue || formInput.value;
    }

    set value(value) {
      const formInput = this.querySelector(`.${this.formInputClassName}`);

      if (this.multiple) {
        this.multipleValues = value;
        formInput.dispatchEvent(new Event('change'));
        return;
      }

      if (formInput) {
        formInput.value = value;
        formInput.dispatchEvent(new Event('change'));
      }
    }

    get name() {
      return this.getAttribute('name');
    }

    set name(value) {
      this.setAttribute('name', value);
    }

    get placeholder() {
      return this.getAttribute('placeholder');
    }

    set placeholder(value) {
      this.setAttribute('placeholder', value);
    }

    get id() {
      return this.getAttribute('id');
    }

    set id(value) {
      this.setAttribute('id', value);
    }

    dispatch = (eventName) => {
      this.dispatchEvent(
        new Event(eventName, {
          bubbles: true,
          composed: true,
        })
      );
    }

    createUID = () => Math.random().toString(16).slice(11);

    bindEvents = () => {
      this.formInput.addEventListener('animationstart', (e) => {
        this.classList.toggle('-has-value', e.animationName === 'onAutoFillStart');
      });

      const changeHandler = (e) => {
        e.stopPropagation();

        const value = this.formInput.value;

        this.classList.toggle('-has-value', this.classList.contains('pz-select') || (value && value.length));

        if (e.type === 'change') {
          this.dispatch('change');

          if (this.onChange) {
            this.onChange();
          }

          if (typeof this.validate === 'function' && this.classList.contains('-dirty')) {
            this.validate();
          }
        }
      };

      this.formInput.addEventListener('change', changeHandler);
      this.formInput.addEventListener('initialize', changeHandler);

      this.formInput.addEventListener('input', () => {
        this.dispatch('input');
        this.dispatch('change');

        if (typeof this.validate === 'function' && this.classList.contains('-dirty')) {
          this.validate();
        }
      });

      this.formInput.addEventListener('focus', () => {
        this.classList.add('-focused');
        this.dispatch('focus');
      });

      this.formInput.addEventListener('blur', () => {
        this.classList.remove('-focused');
        this.dispatch('blur');
      });
    }

    connectedCallback() {
      if (super.connectedCallback) {
        super.connectedCallback();
      }

      this.formInput = this.querySelector('input, select, textarea');
      this.formInput.classList.add(this.formInputClassName);
      this.bindEvents();
      this.formInput.id = `pz-form-input-${this.id || this.createUID()}`;

      if (this.name) {
        this.formInput.name = this.name;
      }

      if (this.placeholder) {
        this.formInput.placeholder = this.placeholder;
      }

      if (this.getAttribute('value')) {
        this.value = this.getAttribute('value');
      }

      this.pzLabel = this.closest('pz-label');
      this.classList.toggle('-labeled', this.pzLabel);
      this.classList.toggle('-inline-label', this.pzLabel?.inline);
      this.dispatch('rendered');
    }
  };

class MobileFiltering {
  constructor(selectedFilters) {
    this.selectedFilters = selectedFilters;
    this.body = $(document.body);
    this.container = this.body.find('.js-list-sidebar');
    this.overview = this.container.find('.js-list-sidebar-overview');
    this.closeIcon = this.container.find('i.js-close-mobile-filtering');
    this.closeButton = this.container.find('pz-button.js-close-mobile-filtering');
    this.openFilterItem = this.container.find('.js-sidebar-filter-item');
    this.closeFilterItem = this.container.find('.js-list-close-accordion');
    this.clearFiltersButton = this.container.find('.js-clear-all-filters');
    this.selectedFilter = document.querySelector('.js-selected-filter');
    this.sidebarFilterItems = document.querySelectorAll('.js-sidebar-filter-item');
    this.bgOverlay = $('.js-bg-overlay');

    if (window.innerWidth < 640) {
      this.init();
    }
  }

  init() {
    this.onCloseButtonClick();
    this.onOpenFilterItem();
    this.onCloseFilterItem();
    this.getSelectedFilterTitle();
    this.onClickFilterTitle();
  }

  open() {
    this.container.addClass('-active');
    this.body.addClass('overflow-hidden');
    this.bgOverlay.removeClass('hidden');
  }

  close() {
    this.container.removeClass('-active');
    this.body.removeClass('overflow-hidden');
    this.bgOverlay.addClass('hidden');
  }

  onCloseButtonClick() {
    this.closeIcon.on('click', () => {
      this.close();
    });

    this.closeButton.on('click', () => {
      this.close();
    });
  }

  onOpenFilterItem() {
    this.openFilterItem.on('click', () => {
      const filterItem = document.querySelector('.js-tab-content-item .js-filter-item');

      filterItem.classList.add('js-filter-edit');
    });
  }

  onCloseFilterItem() {
    this.closeFilterItem.on('click', () => {
      this.close();
    });

    this.bgOverlay.on('click', () => {
      this.close();
    });
  }

  getSelectedFilterTitle() {
    this.sidebarFilterItems.forEach((item) => {
      if (item.classList.contains('-active')) {
        this.selectedFilter.textContent = item.textContent;
      }
    });
  }

  onClickFilterTitle() {
    this.sidebarFilterItems.forEach((item) => {
      item.addEventListener('click', (e) => {
        if (e.target.classList.contains('-active')) {
          this.selectedFilter.textContent = e.target.textContent;
        }
      });
    });
  }
}

export default MobileFiltering;
